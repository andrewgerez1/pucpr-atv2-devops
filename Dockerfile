FROM nginx:alpine

LABEL maintainer="ANDREW GEREZ"

EXPOSE 80

RUN apk add --no-cache curl

ARG INDEX_HTML_URL=https://andrewgerez.github.io/pucpr-atv2-devops/index.html

RUN curl -o /usr/share/nginx/html/index.html $INDEX_HTML_URL

CMD ["nginx", "-g", "daemon off;"]
